# FTDI SIO CBUS GPIO


## FTDI CBUS GPIO patchset by Stefan Agner
[[PATCH 0/2] FTDI CBUS GPIO support](https://lkml.org/lkml/2015/6/20/205)

[[PATCH 1/2] USB: ftdi_sio: add CBUS mode for FT232R devices](https://lkml.org/lkml/2015/6/20/206)

[[PATCH 2/2] gpio: gpio-ftdi-cbus: add driver for FTDI CBUS GPIOs](https://lkml.org/lkml/2015/6/20/207)


## Changes
 * Modified for FT-X chips.
 * With fix for kernel 4.5 and newer.